﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dandago.Settings;

namespace Dandago.AsyncSettings
{
    public class AsyncConfigKeyProvider
    {
        private IAsyncConfigKeyReader reader;

        public AsyncConfigKeyProvider(IAsyncConfigKeyReader reader)
        {
            this.reader = reader;
        }

        public async Task<T> GetAsync<T>(string key, T defaultValue = default(T))
        {
            string valueStr = await this.reader.ReadAsync(key).ConfigureAwait(false);

            return Util.ConvertValue<T>(valueStr, defaultValue);
        }
    }
}
