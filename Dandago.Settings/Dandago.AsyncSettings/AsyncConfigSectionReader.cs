﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dandago.Settings;

namespace Dandago.AsyncSettings
{
    public class AsyncConfigSectionReader : ConfigSectionReader, IAsyncConfigSectionReader
    {
        public Task<T> GetSectionAsync<T>(string configSectionName) where T : class
        {
            var value = this.GetSection<T>(configSectionName);
            return Task.FromResult(value);
        }
    }
}
