﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dandago.Settings;

namespace Dandago.AsyncSettings
{
    public class AsyncEnvironmentVariableReader : EnvironmentVariableReader, IAsyncConfigKeyReader
    {
        public Task<string> ReadAsync(string key)
        {
            var value = this.Read(key);
            return Task.FromResult(value);
        }
    }
}
