﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.AsyncSettings
{
    public interface IAsyncConfigKeyProvider
    {
        Task<T> GetAsync<T>(string key, T defaultValue = default(T));
    }
}
