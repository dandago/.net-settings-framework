﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.AsyncSettings
{
    public interface IAsyncConfigSectionReader
    {
        Task<T> GetSectionAsync<T>(string configSectionName) where T : class;
    }
}
