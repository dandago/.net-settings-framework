﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Dandago.AsyncSettings")]
[assembly: AssemblyDescription("An abstraction of appsettings allowing easy and testable retrieval of settings from arbitrary sources.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Daniel D'Agostino")]
[assembly: AssemblyProduct(".NET Settings Framework Async")]
[assembly: AssemblyCopyright("Copyright © Daniel D'Agostino 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("0355610b-94cb-4ca1-a2d1-415ce809f228")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.4.0.0")]
[assembly: AssemblyFileVersion("1.4.0.0")]
