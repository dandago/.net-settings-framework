﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Settings.ConsoleTestApp
{
    public class MemorySettingReader : IConfigKeyReader
    {

        public string Read(string key)
        {
            switch(key)
            {
                case "timeoutInMilliseconds":
                    return "5000";
                case "enabled":
                    return "true";
                default:
                    return null;
            }
        }
    }
}
