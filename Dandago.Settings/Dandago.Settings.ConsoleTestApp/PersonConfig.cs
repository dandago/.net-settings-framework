﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Settings.ConsoleTestApp
{
    public class PersonConfig : ConfigurationSection
    {
        [ConfigurationProperty("name", DefaultValue=null, IsRequired=true)]
        public string Name
        {
            get
            {
                return (string) this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("age", DefaultValue = 25, IsRequired = false)]
        public int Age
        {
            get
            {
                return (int)this["age"];
            }
            set
            {
                this["age"] = value;
            }
        }
    }
}
