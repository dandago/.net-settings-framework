﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Settings.ConsoleTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //var reader = new MemorySettingReader();
            var reader = new AppSettingReader();
            var provider = new ConfigKeyProvider(reader);

            int timeout = provider.Get<int>("timeoutInMilliseconds", 3000);
            bool enabled = provider.Get<bool>("enabled", false);
            Status status = provider.Get<Status>("status", Status.Off);
            int? nullableNotNull = provider.Get<int?>("nullableNotNull", -1);
            int? nullableNull = provider.Get<int?>("nullableNull", null);

            // ---- Config Sections ----

            //var personConfig = (PersonConfig) ConfigurationManager.GetSection("personConfig");

            var configSectionReader = new ConfigSectionReader();
            var personConfig = configSectionReader.GetSection<PersonConfig>("personConfig");

            // ---- Environment Variables ----

            var environmentVariableReader = new EnvironmentVariableReader();
            var environmentVariableProvider = new ConfigKeyProvider(environmentVariableReader);
            var tmp = environmentVariableProvider.Get<string>("TMP", "notfound");
            var aaa = environmentVariableProvider.Get<string>("aaa", "notfound");
        }
    }
}
