﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Settings.Tests
{
    public class ConfigKeyProviderTests
    {
        #region int tests

        [Test]
        public void Get_IntAvailableWithDefault_AvailableValue()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("5");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int actual = provider.Get<int>("key", -1);

            // assert

            int expected = 5;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_IntAvailableWithoutDefault_AvailableValue()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("5");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int actual = provider.Get<int>("key");

            // assert

            int expected = 5;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_IntMissingWithDefault_Default()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns<string>(null);

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int actual = provider.Get<int>("key", -1);

            // assert

            int expected = -1;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_IntMissingWithoutDefault_DefaultOfType()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns<string>(null);

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int actual = provider.Get<int>("key");

            // assert

            int expected = 0;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_IntInvalidWithDefault_Default()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("notAnInt");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int actual = provider.Get<int>("key", -1);

            // assert

            int expected = -1;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_IntInvalidWithoutDefault_DefaultOfType()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("notAnInt");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int actual = provider.Get<int>("key");

            // assert

            int expected = 0;

            Assert.AreEqual(expected, actual);
        }

        #endregion int tests

        #region Nullable int tests

        [Test]
        public void Get_NullableIntNotNullValueWithDefault_AvailableValue()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("5");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key", -1);

            // assert

            int? expected = 5;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_NullableIntNotNullValueWithoutDefault_AvailableValue()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("5");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key");

            // assert

            int? expected = 5;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_NullableIntNullValueWithDefault_Default()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("null");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key", -1);

            // assert

            int? expected = -1;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_NullableIntNullValueWithoutDefault_DefaultOfType()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("null");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key");

            // assert

            int? expected = null;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_NullableIntMissingWithDefault_Default()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns<string>(null);

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key", -1);

            // assert

            int? expected = -1;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_NullableIntMissingWithoutDefault_Null()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns<string>(null);

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key");

            // assert

            int? expected = null;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_NullableIntInvalidWithDefault_Default()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("notAnInt");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key", -1);

            // assert

            int? expected = -1;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_NullableIntInvalidWithoutDefault_DefaultOfType()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("notAnInt");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            int? actual = provider.Get<int?>("key");

            // assert

            int? expected = null;

            Assert.AreEqual(expected, actual);
        }

        #endregion Nullable int tests

        #region enum tests

        [Test]
        public void Get_EnumAvailableWithDefault_AvailableValue()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("On");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            Status actual = provider.Get<Status>("key", Status.Off);

            // assert

            Status expected = Status.On;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_EnumAvailableWithoutDefault_AvailableValue()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("On");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            Status actual = provider.Get<Status>("key");

            // assert

            Status expected = Status.On;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_EnumMissingWithDefault_Default()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns<string>(null);

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            Status actual = provider.Get<Status>("key", Status.Off);

            // assert

            Status expected = Status.Off;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_EnumMissingWithoutDefault_DefaultOfType()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns<string>(null);

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            Status actual = provider.Get<Status>("key");

            // assert

            Status expected = Status.Invalid;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_EnumInvalidWithDefault_Default()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("notAStatus");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            Status actual = provider.Get<Status>("key", Status.Off);

            // assert

            Status expected = Status.Off;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Get_EnumInvalidWithoutDefault_DefaultOfType()
        {
            // arrange

            var reader = new Mock<IConfigKeyReader>(MockBehavior.Strict);
            reader.Setup(x => x.Read("key")).Returns("notAStatus");

            var provider = new ConfigKeyProvider(reader.Object);

            // act

            Status actual = provider.Get<Status>("key");

            // assert

            Status expected = Status.Invalid;

            Assert.AreEqual(expected, actual);
        }

        #endregion enum tests
    }
}
