﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Dandago.AsyncSettings;

namespace Dandago.Settings.WpfTestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            this.LoadSettings();
        }

        public async void LoadSettings()
        {
            var reader = new AsyncAppSettingReader();
            var provider = new AsyncConfigKeyProvider(reader);

            int timeout = await provider.GetAsync<int>("timeoutInMilliseconds", 3000);
            bool enabled = await provider.GetAsync<bool>("enabled", false);
        }
    }
}
