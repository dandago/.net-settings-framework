﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Settings
{
    public class ConfigKeyProvider : IConfigKeyProvider
    {
        private IConfigKeyReader reader;

        protected IConfigKeyReader Reader
        {
            get
            {
                return this.reader;
            }
        }

        public ConfigKeyProvider(IConfigKeyReader reader)
        {
            this.reader = reader;
        }

        public T Get<T>(string key, T defaultValue = default(T))
        {
            string valueStr = reader.Read(key);

            return Util.ConvertValue<T>(valueStr, defaultValue);
        }
    }
}
