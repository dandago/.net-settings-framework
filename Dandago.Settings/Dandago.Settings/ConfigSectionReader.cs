﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Dandago.Settings
{
    public class ConfigSectionReader : IConfigSectionReader
    {
        public T GetSection<T>(string configSectionName) where T : class
        {
            var configSection = (T) ConfigurationManager.GetSection(configSectionName);
            return configSection;
        }
    }
}
