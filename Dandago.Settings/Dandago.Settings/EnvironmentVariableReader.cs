﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dandago.Settings
{
    public class EnvironmentVariableReader : IConfigKeyReader
    {
        public string Read(string key)
        {
            return Environment.GetEnvironmentVariable(key);
        }
    }
}
