﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.Settings
{
    public interface IConfigKeyProvider
    {
        T Get<T>(string key, T defaultValue = default(T));
    }
}
