﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dandago.Settings
{
    public interface IConfigSectionReader
    {
        T GetSection<T>(string configSectionName) where T : class;
    }
}
